package com.thechalakas.jay.retrofit;
/*
 * Created by jay on 23/09/17. 12:07 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeadPhoneSend {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("WarehouseId")
    @Expose
    private Integer warehouseId;
    @SerializedName("Warehouse")
    @Expose
    private Warehouse warehouse;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

}